@ecommerce
Feature: User checks Phone price in different ecommerce web sites

  @hashook
  Scenario Outline: Verify user can see price of phone
    Given user open "<sitename>" eCommerce website
    When user search for phone with details "<modelName>" and "<ramSize>" and "<color>"
    Then verify user see price of phone

    Examples: 
      | sitename | modelName | ramSize | color  |
      | amazon   | iPhone XR |      64 | Yellow |
      | flipkart | iPhone XR |      64 | Yellow |
      | amazon   | iPhone 11 |      32 | Black  |
      | flipkart | iPhone 11 |      32 | Black  |

  Scenario Outline: Verify user can compare prices in different eCommerce sites
    Given user open "<sitename>" eCommerce website 
    When user search for phone with details "<modelName>" and "<ramSize>" and "<color>"
    Then verify user can compare prices

    Examples: 
      | sitename | modelName | ramSize | color |
      | flipkart | iPhone SE |      64 | Gold  |
      | amazon   | iPhone SE |      64 | Gold  |
