-Generate report-Completed
- create separate file for hooks: Completed
-Implement 1 scenario without scenario outline:Completed
-Move feature file outside SRC: Completed
-Make use of tag in runner file:Completed
-In Step Definition, Instead of ^user open eCommerce website \"([^\"]*)\"$" it should be ^user open eCommerce website {String}$. Can be achieved using cucumber expression dependency.
:Completed

##################
17/08
-Getting garbage log value on console. Use monochrome in testrunner file.-Completed
-Method name should not be same as step name. Follow the same naming convention we follow in wells.-Completed.Used auto generated code earlier
-Why do we need throwable exception for method verify_user_can_compare_prices()?-As I am using auto generated steps I forgot to commit.

