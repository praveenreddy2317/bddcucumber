package com.stepdefinations;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ecommerce.util.WebDriverManager;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ScenariosWithOutOutline {
  private WebDriver driver;
	 @Given("user open {string} browser")
	    public void user_open_browser(String browserName) {
		 if(browserName.equalsIgnoreCase("chrome"))
	    	driver=WebDriverManager.getInstance().getDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
	    }

	    @When("user open {string} site")
	    public void user_open_site(String url) {
	       
	    	driver.get(url);
	    }

	    @Then("user navigate to home page")
	    public void user_see_home_page() {
			Assert.assertTrue("User is on Home Page", WebDriverManager.getInstance().getDriver().findElement(By.xpath("//span[text()='Infostretch']")).isDisplayed());
			driver.quit();

	    }
	    
}
