package com.stepdefinations;

import org.junit.Assert;

import com.ecommerce.pages.AmazonHomePage;
import com.ecommerce.pages.AmazonProductDetailsPage;
import com.ecommerce.pages.FlipkartHomePage;
import com.ecommerce.pages.FlipkartProductDetailsPage;
import com.ecommerce.util.Phone;
import com.ecommerce.util.WebDriverManager;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PhonePriceSteps {

	Phone iPhone;
	FlipkartHomePage flipkartHomePage;
	FlipkartProductDetailsPage flipkartProductDetailsPage;

	AmazonHomePage amazonHomePage;
	AmazonProductDetailsPage amazonProductDetailsPage;
	String siteName;
	public static Float amazonPhonePrice;
	public static Float flipkartPhonePrice;

	@Given("user open {string} eCommerce website")
	public void openEcommerceWebsite(String siteName) {
		this.siteName = siteName;

		if (siteName.equalsIgnoreCase("amazon")) {

			amazonHomePage = new AmazonHomePage(WebDriverManager.getInstance().getDriver());
			amazonHomePage.launchHomePage();

		}

		if (siteName.equalsIgnoreCase("flipkart")) {
			flipkartHomePage = new FlipkartHomePage(WebDriverManager.getInstance().getDriver());
			flipkartHomePage.launchHomePage();
		}

	}

	@When("user search for phone with details {string} and {string} and {string}")
	public void searchForPhone(String modelName, String ramSize, String color) {
		iPhone = new Phone(modelName, ramSize, color);

		if (siteName.equalsIgnoreCase("amazon")) {
			amazonHomePage.searchProduct(iPhone);
		}

		if (siteName.equalsIgnoreCase("flipkart")) {
			flipkartHomePage.searchProduct(iPhone);
		}
	}

	@Then("verify user see price of phone")
	public void seePhonePrice() {
		String phonePrice = null;

		if (siteName.equalsIgnoreCase("amazon")) {
			amazonProductDetailsPage = new AmazonProductDetailsPage(WebDriverManager.getInstance().getDriver());
			phonePrice = amazonProductDetailsPage.getProductPrice(iPhone);
			amazonPhonePrice = Float.parseFloat(phonePrice);
		}

		if (siteName.equalsIgnoreCase("flipkart")) {
			flipkartProductDetailsPage = new FlipkartProductDetailsPage(WebDriverManager.getInstance().getDriver());
			phonePrice = flipkartProductDetailsPage.getProductPrice(iPhone);
			flipkartPhonePrice = Float.parseFloat(phonePrice);
		}
		Assert.assertTrue("Phone price is displayed to the user", phonePrice != null && !phonePrice.isEmpty());
	}

	@Then("verify user can compare prices")
	public void comparePhonePrices() {

		 seePhonePrice();
		
		if(amazonPhonePrice !=null && flipkartPhonePrice != null) {
		
		int compPrice = Float.compare(flipkartPhonePrice, amazonPhonePrice);
		if (compPrice > 0) {

			Assert.assertTrue("Price is less in Amazon", compPrice > 0);

		} else if (compPrice < 0) {

			Assert.assertTrue("Price is less in Flipkart", compPrice < 0);
		} else {

			Assert.assertTrue("Price is same in Flipkart & Amazon", compPrice == 0);
		}

	}
	}

}
