package com.hooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.ecommerce.util.WebDriverManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;



public class BaseHook {
	
	private static WebDriver driver;
	
	@Before
	public void beforeLogin() {
	
		 driver=WebDriverManager.getInstance().getDriver();
		
	}
	
	
	@After
	public void afterLogOut() {
		if (driver != null) {
			try {
				driver.quit();
			} catch (WebDriverException e) {

				e.printStackTrace();
			}
		}
	}

}
