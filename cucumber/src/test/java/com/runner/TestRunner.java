package com.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;



@RunWith(Cucumber.class)
@CucumberOptions(
		 features = "features"
		 ,glue={"com/stepdefinations"},		 
		 plugin = {"pretty", "html:target/cucumber-reports/report.html"},
		 tags = "@nohook or @hashook",
		 monochrome=true
	

		 )


public class TestRunner {
	
	

}


