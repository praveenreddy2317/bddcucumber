package com.ecommerce.util;

public class Phone {
	
	private String modelName;
	private String ramSize;
	private String color;
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getRamSize() {
		return ramSize;
	}
	public void setRamSize(String ramSize) {
		this.ramSize = ramSize;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Phone(String modelName,String ramSize,String color) {
		this.modelName=modelName;
		this.ramSize=ramSize;
		this.color=color;
	}

}
