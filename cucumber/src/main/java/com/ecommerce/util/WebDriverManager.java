package com.ecommerce.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverManager {

	private static WebDriver driver;
	private static WebDriverManager wdm = new WebDriverManager();

	
	public WebDriver getDriver() {
				
		return (driver == null) ? createDriver() : driver;
	}
	
	
	private WebDriverManager() {
	}

	 public static WebDriverManager getInstance( ) {
	      return wdm;
	 }

	
	private WebDriver createDriver() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return driver;
	}
	
	public void closeDriver() {
		
		driver.quit();
	}

}
