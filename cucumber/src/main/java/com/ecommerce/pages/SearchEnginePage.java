package com.ecommerce.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ecommerce.util.Phone;

public class SearchEnginePage {
	
	
	
	private WebDriver driver;
	private static String baseUrl="https://www.google.com/";
	
	@FindBy(xpath="//input[@title='Search']")
	WebElement inputSearch;
	
	@FindBy(xpath="//input[@value='Go']")
	WebElement buttonSearch;
	
	public SearchEnginePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void launchHomePage() {
		driver.get(baseUrl);
	}
	
	public void searchWord(String word) {
		inputSearch.clear();
		inputSearch.sendKeys(word);
		buttonSearch.click();
	}

}
