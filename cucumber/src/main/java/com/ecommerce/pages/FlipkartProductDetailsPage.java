package com.ecommerce.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.ecommerce.util.Phone;

public class FlipkartProductDetailsPage {
	
	private WebDriver driver;
	
	public FlipkartProductDetailsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getProductPrice(Phone p) {
		String locator="//div[contains(text(),'%s') and contains(text(),'%s') and contains(text(),'%s')]/parent::div/following-sibling::div//div[contains(text(),'₹')]";
		String locatorPrice=String.format(locator, p.getModelName(),p.getColor(),p.getRamSize());
		String textProductPrice=driver.findElement(By.xpath(locatorPrice)).getText();
		return textProductPrice.replaceAll("[^0-9]", "");
	}

}
