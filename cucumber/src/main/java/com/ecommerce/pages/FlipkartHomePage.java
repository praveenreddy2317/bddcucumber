package com.ecommerce.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ecommerce.util.Phone;

public class FlipkartHomePage {
	
	private WebDriver driver;
	private static String baseUrl="https://flipkart.com";
	
	@FindBy(xpath="//input[contains(@title,\"Search for products\")]")
	WebElement inputSearch;
	
	@FindBy(xpath="//input[contains(@title,\"Search for products\")]/ancestor::form//button")
	WebElement buttonSearch;
	
	@FindBy(xpath="//form[@autocomplete='on']/ancestor::div[@tabindex=-1]/div/button")
	WebElement buttonClose;

	public FlipkartHomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void launchHomePage() {
		driver.get(baseUrl);
		if(buttonClose.isDisplayed()) {
			closePopup();
		
		}
	}
	
	public void searchProduct(Phone p) {
		inputSearch.clear();
		inputSearch.sendKeys(p.getModelName()+" "+p.getRamSize()+" "+p.getColor());
		buttonSearch.click();
	}
	
	public void closePopup() {
		buttonClose.click();
	}
}
