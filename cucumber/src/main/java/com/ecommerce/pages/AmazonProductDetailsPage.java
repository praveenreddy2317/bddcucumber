package com.ecommerce.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.ecommerce.util.Phone;

public class AmazonProductDetailsPage {
	
	private WebDriver driver;
	
	public AmazonProductDetailsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getProductPrice(Phone p) {
		String locator="//span[contains(text(),'%s') and contains(text(),'%s') and contains(text(),'%sGB')]/ancestor::h2/ancestor::div[@class='sg-row']//span[@class='a-price-whole'] ";
		String locatorPrice=String.format(locator, p.getModelName(),p.getColor(),p.getRamSize());
		String textProductPrice=driver.findElement(By.xpath(locatorPrice)).getText();
		return textProductPrice.replaceAll("[^0-9]", "");
	}

}
