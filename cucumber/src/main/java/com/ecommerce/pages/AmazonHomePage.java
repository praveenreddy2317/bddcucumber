package com.ecommerce.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ecommerce.util.Phone;

public class AmazonHomePage {

	
	private WebDriver driver;
	private static String baseUrl="http://www.amazon.in/";
	
	@FindBy(xpath="//input[@id='twotabsearchtextbox']")
	WebElement inputSearch;
	
	@FindBy(xpath="//input[@value='Go']")
	WebElement buttonSearch;
	
	public AmazonHomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void launchHomePage() {
		driver.get(baseUrl);
	}
	
	public void searchProduct(Phone p) {
		inputSearch.clear();
		inputSearch.sendKeys(p.getModelName()+" "+p.getRamSize()+" "+p.getColor());
		buttonSearch.click();
	}
}
